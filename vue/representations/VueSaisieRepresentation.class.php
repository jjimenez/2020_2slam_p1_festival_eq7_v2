<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\metier\Lieu;
use modele\dao\LieuDAO;
use modele\metier\Groupe;
use modele\dao\GroupeDAO;

ini_set('display_errors', 'on');

class VueSaisieRepresentation extends VueGenerique {

    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>

 <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie           
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>
                    
                    <tr class="ligneTabNonQuad">
                    <td>Lieu: </td>
                    <td> <select name="lieu">
                            <?php
                            $lesLieux = LieuDAO::getAll();
                            
                            for ($i = 0; $i < sizeof($lesLieux) ; $i = $i + 1) {
                                $selected = "";
                                if ($lesLieux[$i]->getNom() == $this->uneRepresentation->getLieu()->getNom()) {
                                    //$selected = "selected=\"selected\"";
                                
                                ?>
                            <option selected="selected"><?=$lesLieux[$i]->getNom()?></option>
                                <?php
                            }else{
                                ?>
                            <option><?=$lesLieux[$i]->getNom()?></option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                                <tr class="ligneTabNonQuad">
                    <td>Groupe: </td>
                    <td> <select name="lieu">
                            <?php
                            $lesGroupes = GroupeDAO::getAll();
                            foreach ($lesGroupes as $unGroupe) {
                                ?>
                                <option value><?= $unGroupe->getNom() ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                    
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getDate() ?>" name="date" 
                               size="50" maxlength="45"></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td> Heure de début*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heureDebut" size="50" 
                               maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de fin*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heureFin" 
                               size="50" maxlength="45"></td>
                </tr>

                
            </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }
    
        public function setUneRepresentation (Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }
     public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

    
}
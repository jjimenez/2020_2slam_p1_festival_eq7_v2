<?php

namespace modele\dao;

use modele\metier\Lieu;
use \PDO;

/**
 * Description of AttributionDAO
 * Classe métier :  Attribution
 * @author prof
 * @version 2017
 */
class LieuDAO {
    
        
        //Instancie un objet de la classe Lieu à partir d'un enregistrement de la table LIEU

        protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $nom = $enreg['NOM'];
        $adresse = $enreg['ADRESSE'];
        $capacite = $enreg['CAPACITEACCUEIL'];
        // instancier l'objet Lieu
        $objetMetier = new Lieu($id, $nom, $adresse, $capacite);     
        return $objetMetier;
    }

     //Valorise les paramètres d'une requête préparée avec l'état d'un objet Lieu

    protected static function metierVersEnreg(Lieu $objetMetier, PDOStatement $stmt) {
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':nom', $objetMetier->getNom());
        $stmt->bindValue(':adresse', $objetMetier->getAdresse());
        $stmt->bindValue(':capacite accueil', $objetMetier->getCapacite());
    }
    
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
     public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }

        return $objetConstruit;
    }
}
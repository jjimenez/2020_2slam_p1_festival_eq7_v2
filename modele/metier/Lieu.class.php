<?php
namespace modele\metier;

/**
 * Description of Groupe
 * un groupe musical se produisant au festival
 * @author eleve
 */
class Lieu {
    
    private $id;
    private $nom;
    private $adresse;
        /**
     * @var int
     */
    private $capacite;
    
        function __construct($id, $nom, $adresse, $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }
    
    function getId() {
        return $this->id;
    }
        function getNom() {
        return $this->nom;
    }
        function getAdresse() {
        return $this->adresse;
    }
        function getCapacite() {
        return $this->capacite;
    }
    
        function setId() {
         $this->id;
    }
        function setNom() {
         $this->nom;
    }
            function setAdresse() {
         $this->adresse;
    }
                function setCapacite() {
         $this->capacite;
    }
    
}
?>